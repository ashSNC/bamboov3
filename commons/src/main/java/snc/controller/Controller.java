package snc.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class Controller
{
    private TurretController model;
    private ControllerWindow view;

    public Controller(TurretController model, ControllerWindow view)
    {
        this.model = model;
        this.view = view;

        // listeners for drop-down and button
        view.addCommandSubmitListener(new CommandSubmitListener());
        view.addComboBoxSelectionListener(new CommandSelectionListener());

        // Bandaid fix - trigger listener upon startup to avoid null pointer exception
        view.SteeringModeDropDown.setSelectedIndex(1);

    }

    public class CommandSubmitListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e) {
            model.setCurrentSteeringMode();
        }
    }

    public class CommandSelectionListener implements ItemListener
    {
        @Override
        public void itemStateChanged(ItemEvent command)
        {

            if(command.getStateChange() == ItemEvent.SELECTED)
                model.updateSteeringModeSelection(command.getItem().toString());
        }
    }
}
