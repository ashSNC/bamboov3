package snc.controller;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ItemListener;

public class ControllerWindow {
    public JPanel MainPanel;
    public JComboBox SteeringModeDropDown;
    private JButton SteeringModeSubmitButton;
    private TurretController model;

    public ControllerWindow(TurretController model)
    {
        this.model = model;
    }

    public void addCommandSubmitListener(ActionListener listener) {
        SteeringModeSubmitButton.addActionListener(listener);
    }

    public void addComboBoxSelectionListener(ItemListener listener)
    {
        SteeringModeDropDown.addItemListener(listener);
    }


    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        MainPanel = new JPanel();
        MainPanel.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
        SteeringModeDropDown = new JComboBox();
        SteeringModeDropDown.setEditable(false);
        SteeringModeDropDown.setEnabled(true);
        MainPanel.add(SteeringModeDropDown, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        SteeringModeSubmitButton = new JButton();
        SteeringModeSubmitButton.setText("Submit");
        MainPanel.add(SteeringModeSubmitButton, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return MainPanel;
    }
}
