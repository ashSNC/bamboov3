package snc.controller;

import snc.turret.base.ITurret;
import snc.turret.enums.GimbalSteeringMode;
import snc.turret.mx.TurretMX;

import java.io.IOException;
import java.util.Properties;

public class TurretController implements IControllerBase
{
    private GimbalSteeringMode currentSelection_SteeringMode;
    private ITurret turretMX;

    public TurretController()
    {
        initializeTurret();
    }

    public void initializeTurret()
    {
        turretMX = new TurretMX();

        try
        {
            turretMX.start(new Properties());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setCurrentSteeringMode()
    {
        turretMX.setGimbalMode(currentSelection_SteeringMode);
    }

    public void updateSteeringModeSelection(String e)
    {
        currentSelection_SteeringMode = GimbalSteeringMode.valueOf(e);
    }





}