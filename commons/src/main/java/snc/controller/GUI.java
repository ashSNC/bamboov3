package snc.controller;

import javax.swing.*;

public class GUI implements Runnable {
    private TurretController model;
    private ControllerWindow view;
    private Controller controller;

    @Override
    public void run() {
        instantiateAll();
    }

    private void instantiateAll()
    {
        model = new TurretController();
        view = new ControllerWindow(model);
        GUICommandPopulator.PopulateCommands.PopulateSteeringModeCommands(view);

        controller = new Controller(model, view);
        JFrame frame = new JFrame("MX-15 Controller");
        frame.setContentPane(view.MainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
