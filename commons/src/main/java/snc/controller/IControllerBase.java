package snc.controller;

import java.util.Collection;

public interface IControllerBase
{
    void initializeTurret();

    void setCurrentSteeringMode();
    void updateSteeringModeSelection(String steeringMode);

}
