package snc.controller;

import snc.turret.enums.GimbalSteeringMode;

public class GUICommandPopulator
{
    public static class PopulateCommands
    {

        // Steering Mode Setting
        public static void PopulateSteeringModeCommands(ControllerWindow view)
        {
            for(GimbalSteeringMode mode: GimbalSteeringMode.values())
            {
                view.SteeringModeDropDown.addItem(mode.name());
            }



        }

        // TODO: Other commands
    }

}