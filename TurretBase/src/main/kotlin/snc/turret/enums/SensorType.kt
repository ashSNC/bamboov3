package snc.turret.enums

enum class SensorType {
    None,
    TV_EOW,
    EON,            // MX Only
    HDIR_IR,
    SWIR,           // FLIR Only
    Blended_Fused,
    SeeSpot,        // MX Only
    EOWDaylight,
    EOWLowlight,
    EONDaylight,
    EONLowlight,
    EONSWIR,
    Increment,
    Decrement,
    Wrap;       // For MX, most likely want to send this for Sub-vic control

    //TODO - If we ever want to skip values in this enum, we need to:
    // remove this getValue() method,
    // add a Byte constructor,
    // and declare individual values
    // like GimbalSteeringMode
    fun getValue(): Byte { return this.ordinal.toByte();}
}