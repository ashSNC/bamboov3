package snc.turret.enums

enum class SensitivityGainLevelAdjustMode {
    IncreaseGainAtFineSlowerRate,   // FLIR Only
    IncreaseGainAtCoarseFasterRate, // FLIR Only
    DecreaseGainAtFineSlowerRate,   // FLIR Only
    DecreaseGainAtCoarseFasterRate, // FLIR Only
    StopGainChange,
    IncreaseLevelAtFineSlowerRate,  // FLIR Only
    IncreaseLevelAtCoarseFasterRate,    // FLIR Only
    DecreaseLevelAtFineSlowerRate,  // FLIR Only
    DecreaseLevelAtCoarseFasterRate,    // FLIR Only
    StopLevelChange,    // FLIR Only
    IncreaseSensitivityOrLevel, // MX Only
    DecreaseSensitivityOrLevel; // MX Only

    //TODO - If we ever want to skip values in this enum, we need to:
    // remove this getValue() method,
    // add a Byte constructor,
    // and declare individual values
    // like GimbalSteeringMode
    fun getValue(): Byte { return this.ordinal.toByte();}
}