package snc.turret.enums

enum class LaserState {
    Unknown,
    None,
    Firing,
    NearCutout,
    InCutout,
    Warming,
    Ready,
    FiringAndNearCutout;

    //TODO - If we ever want to skip values in this enum, we need to:
    // remove this getValue() method,
    // add a Byte constructor,
    // and declare individual values
    // like GimbalSteeringMode
    fun getValue(): Byte { return this.ordinal.toByte();}
}