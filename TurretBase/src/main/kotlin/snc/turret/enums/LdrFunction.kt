package snc.turret.enums

enum class LdrFunction(val value: Byte) {
    RangeManual(0x1),
    RangeAuto(0x2),
    Designator(0x3);
}