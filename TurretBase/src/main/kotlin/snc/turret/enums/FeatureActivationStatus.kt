package snc.turret.enums

enum class FeatureActivationStatus(val value: Byte) {
    Activate (0x00),
    Deactivate (0x01),
    Toggle (0x02);
}