package snc.turret.enums

enum class ActivatableFeature(val value: Byte) {
    ControlXfer (0x01),
    DefaultFocus (0x02),
    GeopointAtTarget (0x03),
    HazePenetration (0x04),
    Hibernate (0x05),
    LiHotButton (0x06),
    Nuc (0x07),
    PassiveGeopoint (0x08),
    PrfToggle (0x09),
    RateAid (0x0a),
    Slaving (0x0b),
    VcrRecord (0x0c),
    ZeroizeLdCodes (0x0d),
    GyroNull_AutoNull (0x0e),
    SlavedFov (0x0f),
    Fit (0x10);
}