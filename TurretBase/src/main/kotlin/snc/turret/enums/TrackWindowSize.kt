package snc.turret.enums

enum class TrackWindowSize(val value: Byte) {
    Stop(0),    //FLIR Only
    DecreaseGateSize(1),
    IncreaseGateSize(2),
    WrapGateSize(3);    // MX Only
}