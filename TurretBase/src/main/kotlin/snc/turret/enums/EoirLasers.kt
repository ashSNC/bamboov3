package snc.turret.enums

enum class EoirLasers @ExperimentalUnsignedTypes constructor(val value: UInt) {
    None(0x0u),
    LaserDesignator(0x000000ffu),
    LaserIlluminatorNIR(0x0000ff00u),
    LaserIlluminatorGreen(0x00ff0000u),
    LaserRangeFinder(0xff000000u);
}