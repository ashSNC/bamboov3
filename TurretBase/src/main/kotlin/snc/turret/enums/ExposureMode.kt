package snc.turret.enums

enum class ExposureMode(val value: Byte) {
    AutoToggle(0),
    AutomaticMode(1),
    EnhancedMode(2),   // FLIR Only
    ManualMode(3),
    EnhancedModeDirect(4), // FLIR Only
    AdaptiveLocalAreaProcessingMode(5),    // FLIR Only
}