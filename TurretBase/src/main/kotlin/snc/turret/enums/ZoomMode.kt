package snc.turret.enums

enum class ZoomMode {
    ZoomOff,
    ZoomIn_Increment,   // Used for stepped zoom sensors
    ZoomOut_Decrement,  // Used for stepped zoom sensors
    ZoomIndex,          // MX Only, Set the zoom level index
    ZoomFov,            // FLIR Only
    ZoomExtender,       // FLIR Only
    ZoomIncreaseByGain, //MX Only, Used for continuous zoom sensors to zoom in using the zoom gain
    ZoomDecreaseByGain, //MX Only, Used for continuous zoom sensors to zoom out using the zoom gain
    SetZoomRateUsingZoomGain;   //TODO - I don't know what this does

    //TODO - If we ever want to skip values in this enum, we need to:
    // remove this getValue() method,
    // add a Byte constructor,
    // and declare individual values
    // like GimbalSteeringMode
    fun getValue(): Byte { return this.ordinal.toByte();}
}