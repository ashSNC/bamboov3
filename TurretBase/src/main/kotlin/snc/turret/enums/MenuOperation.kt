package snc.turret.enums

enum class MenuOperation {
    ToggleMenuOnOff,
    SelectMenuItemIfMenuOpen,   // FLIR Only, Will open the command menu if the main menu is closed
    SelectNextMenuItem,         // MX Only
    SelectPrevMenuItem,         // MX Only
    IncrementSelectedItem,      // MX Only, Use when the arrows point inward
    DecrementSelectedItem,      // MX Only, Use when the arrows point inward
    Enter;                      // MX Only, What does this do?

    //TODO - If we ever want to skip values in this enum, we need to:
    // remove this getValue() method,
    // add a Byte constructor,
    // and declare individual values
    // like GimbalSteeringMode
    fun getValue(): Byte { return this.ordinal.toByte();}
}