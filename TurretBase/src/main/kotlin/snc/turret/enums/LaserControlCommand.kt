package snc.turret.enums

enum class LaserControlCommand (val value: Byte) {
    Arm(0x1),
    Fire(0x2),
    Shutdown(0x3),
    Disarm(0x6)
}