package snc.turret.enums

data class LaserSpotTrackerStatus(  val selectedPrfCode: UShort,
                                    val isLstModeEnabled: Boolean,
                                    val lstState: LstState,
                                    val timeTag: UInt)