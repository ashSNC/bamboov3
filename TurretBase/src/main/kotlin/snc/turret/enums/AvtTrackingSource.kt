package snc.turret.enums


enum class AvtTrackingSource(val value: Byte) {
    Off(0),
    Eow(1),
    Ir(2),
    Eon(3),
    Vic(4),
    Bld(6),
    See(7);
}