package snc.turret.enums

enum class GimbalSteeringMode(val value: Byte) {
    Stowed(0x1),
    Manual(0x2),
    RateAid(0x3),
    AutoScan(0x4),
    Auto(0x5),
    Forward(0x6),
    AutoVideoTrackerLocked(0x7),
    Rate(0x8),
    Position(0xA),
    AutoAid(0xB),
    PositionEarth(0xC),
    GeoScan(0xD),
    LaserSpotTracker(0xE),
    AVT(0xF);

}