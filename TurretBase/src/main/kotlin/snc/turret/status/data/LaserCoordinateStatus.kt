package snc.turret.status.data

import snc.turret.enums.LaserCoordinateSource

data class LaserCoordinateStatus @ExperimentalUnsignedTypes constructor(val source: LaserCoordinateSource,
                                                                        val slantRangeMeters: Double,
                                                                        val latitudeDegrees: Double,
                                                                        val longitudeDegrees: Double,
                                                                        val altitudeMeters: Double,
                                                                        val isValid: Boolean,
                                                                        val timeTag: ULong)
