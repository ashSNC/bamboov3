package snc.turret.status.data

import snc.turret.enums.ExposureMode
import snc.turret.enums.SceneMode
import snc.turret.enums.SensorType

data class SensorPayloadStatus @ExperimentalUnsignedTypes constructor(val selectedPayload: SensorType,
                                                                      val selectedSubPayload: SensorType,
                                                                      val horizontalFOV: Double,
                                                                      val verticalFOV: Double,
                                                                      val zoomLevel: Double,
                                                                      val gainMode: ExposureMode,
                                                                      val sceneSelection: SceneMode,
                                                                      val isGeoFocusEnabled: Boolean,
                                                                      val focusLevel: Double,
                                                                      val isWhiteHot: Boolean,
                                                                      val timeTag: ULong)
