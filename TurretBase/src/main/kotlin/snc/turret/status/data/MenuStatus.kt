package snc.turret.status.data

data class MenuStatus(val isMenuOpen: Boolean, val timeTag: ULong)