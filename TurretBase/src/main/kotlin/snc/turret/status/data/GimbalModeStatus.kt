package snc.turret.status.data

import snc.turret.enums.GimbalSteeringMode

data class GimbalModeStatus @ExperimentalUnsignedTypes constructor(val gimbalMode: GimbalSteeringMode,
                                                                   val isElevationSteeringInverted: Boolean,
                                                                   val timeTag: ULong)