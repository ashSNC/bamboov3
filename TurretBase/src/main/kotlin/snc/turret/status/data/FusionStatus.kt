package snc.turret.status.data

import snc.turret.enums.SensorType

data class FusionStatus @ExperimentalUnsignedTypes constructor(val firstSelectedPayload: SensorType,
                                                               val secondSelectedPayload: SensorType,
                                                               val fusionPercentage: Double,
                                                               val timeTag: ULong)