package snc.turret.status.data

data class SensorPositionStatus @ExperimentalUnsignedTypes constructor(val latitude: Double,
                                                                       val longitude: Double,
                                                                       val altitudeFeetMSL: Double,
                                                                       val course: Double,
                                                                       val speedMetersPerSecond: Double,
                                                                       val isValid: Boolean,
                                                                       val timeTag: ULong)