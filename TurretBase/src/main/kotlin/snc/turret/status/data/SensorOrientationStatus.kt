package snc.turret.status.data

data class SensorOrientationStatus (val azimuth: Double, val elevation: Double, val timeTag: ULong)