package snc.turret.status.data

data class SensorTargetStatus @ExperimentalUnsignedTypes constructor(val latitude: Double,
                                                                     val longitude: Double,
                                                                     val altitudeFeetMSL: Double,
                                                                     val slantRangeMeters: Double,
                                                                     val course: Double,
                                                                     val speedMetersPerSecond: Double,
                                                                     val isValid: Boolean,
                                                                     val timeTag: ULong)