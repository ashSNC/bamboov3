package snc.turret.status.data

import snc.turret.enums.*

data class LaserStatus @ExperimentalUnsignedTypes constructor(val laserWavelength: EoirLasers,
                                                              val laserState: LaserState,
                                                              val isInAir: Boolean,
                                                              val laserMode: LaserMode,
                                                              val laserOperationalMode: LaserOperationalMode,
                                                              val laserErrors: LaserErrors,
                                                              val prfCode: UShort,
                                                              val laserFunction: LaserFunction,
                                                              val timeTag: ULong)
