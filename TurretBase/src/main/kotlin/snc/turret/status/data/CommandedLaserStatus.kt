package snc.turret.status.data

import snc.turret.enums.EoirLasers
import snc.turret.enums.LaserControlCommand


data class CommandedLaserStatus @ExperimentalUnsignedTypes constructor(val undesignatePressed: Boolean,
                                                                       val lastUndesignatePressedTime: ULong,
                                                                       val missionGripTriggerPressed: Boolean,
                                                                       val currentLdOfpCommand: LaserControlCommand,
                                                                       val currentLiNirOfpCommand: LaserControlCommand,
                                                                       val currentLiGreenOfpCommand: LaserControlCommand,
                                                                       val selectedWavelength: EoirLasers,
                                                                       val lrfArmed: Boolean,
                                                                       val laserFire: Boolean,
                                                                       val firedWithUndesignate: Boolean,
                                                                       val lrfIsOneShot: Boolean,
                                                                       val lastWavelengthSelectTiome: ULong,
                                                                       val lastLaserStartFireTime: ULong)