package snc.turret.base;


import snc.turret.enums.*;
import snc.turret.status.data.*;

import java.io.IOException;
import java.util.Properties;

public interface ITurret {

    public void start(Properties properties) throws IOException;
    public void stop();

    //Commands
    public void addLaserCode(short laserCode, boolean useSpecificSlot, byte slotNumber);

    //TODO - In the A-29 MX Server, it looks like Altitude is overwritten to an invalid value to force DTED use.
    // Investigate whether we want to change this to allow user specified altitude.
    public void cueSensor(double latitudeDegrees, double longitudeDegrees, double altitudeMeters); //TODO - Add course, groundspeed, and timetag

    public void operateMenu(MenuOperation menuOperation);
    public void setAVT_Size(TrackWindowSize windowSize);
    public void setAVT_TrackingSource(AvtTrackingSource avtTrackingSource);
    public void setExposureMode(ExposureMode newMode);
    public void setFocus(byte focusValue); //Minimum value 0 and maximum value 255
    public void setGainLevelAdjustMode(SensitivityGainLevelAdjustMode gainLevelAdjustMode);
    public void setGimbalMode(GimbalSteeringMode newMode);
    public void setLDRMode(LdrFunction ldrMode);
    public void setVIC(SensorType vicSensorType); //VIC stands for Video In Command (Primary sensor video type)
    public void setZoom(ZoomMode zoomMode, double zoomFovAngle, double zoomContinuousFraction, int zoomIndex);  // TODO - Break this down into multiple overloads
    public void slewSensor(double azimuth, double elevation);
    public void stopSlew();

    public void setFeatureActivation(ActivatableFeature feature, FeatureActivationStatus featureStatus); // Catch-all for other sensor commands

    //Status
    public CommandedLaserStatus getCommandedLaserStatus();
    public FusionStatus getFusionStatus();
    public GimbalModeStatus getGimbalSteeringModeStatus();
    public LaserCoordinateStatus getLaserCoordinateStatus();
    public LaserSpotTrackerStatus getLaserSpotTrackerStatus();
    public LaserStatus getLaserStatus();
    public MenuStatus getMenuStatus();
    public SensorOrientationStatus getSensorOrientationStatus();
    public SensorPayloadStatus getSensorPayloadStatus();
    public SensorPositionStatus getSensorPositionStatus();
    public SensorTargetStatus getSensorTargetStatus();


}
