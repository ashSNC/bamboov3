package snc.turret.base;

import java.io.*;
import java.net.*;
import java.util.logging.*;
import java.util.Properties;

public abstract class TurretBase implements ITurret {
    private InetAddress serverAddress;
    private int serverPort;
    private Socket serverConnection;
    protected Logger logger = Logger.getLogger(String.valueOf(getClass()));

    public void start(Properties properties) throws IOException {
        readProperties(properties);

        createServerConnection();
    }

    public void stop(){
        if(serverConnection != null) {
            try {
                logger.info("stopping: closing connection to server");
                serverConnection.close();
            } catch (IOException e) {
            }
        }
    }

    private void readProperties(Properties properties) throws UnknownHostException {
        serverAddress = InetAddress.getByName(properties.getProperty("ServerAddress", "127.0.0.1"));
        serverPort = Integer.parseInt(properties.getProperty("ServerPort", "1234"));
    }

    private void createServerConnection() {
        try {
            serverConnection = new Socket(serverAddress, serverPort);

            logger.info(String.format("Server connection created targeting IP address %1$s, port %2$s", serverAddress, serverPort));
        }
        catch (IOException e){
            logger.severe(String.format("Could not connect to server at IP address %1$s, port %2$s", serverAddress, serverPort));
            // TODO - What should we do here? Retry connection on a timer?
        }
    }

    protected void writeCommandBytes(byte[] bytes) {
        if(serverConnection != null) {
            try {
                OutputStream out = serverConnection.getOutputStream();
                out.write(bytes);
                out.flush();
            }catch (IOException e){
                logger.severe(String.format("IOException writing command to server at IP address %1$s, port %2$s", serverAddress, serverPort));

                createServerConnection(); //Attempt to restart the connection;
            }
        }
    }

    protected void writeCommandString(String commandString) throws IOException {
        if(serverConnection != null) {
            OutputStream out = serverConnection.getOutputStream();
            PrintWriter writer = new PrintWriter(out, true);
            writer.println(commandString);
        }
    }

    protected void writeCommandObject(Serializable obj) throws IOException {
        if(serverConnection != null) {
            ObjectOutputStream out = new ObjectOutputStream(serverConnection.getOutputStream());
            out.writeObject(obj);
            out.flush();
        }
    }
}
