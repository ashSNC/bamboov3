package snc.turret.mx.status

data class EonStatus(override var timeTag: Long,
                     var exposureModeManual: Boolean,
                     var GeoFocusModeOn: Boolean,
                     var ImagePolarityBlackHot: Boolean,
                     var FocusTrimOn: Boolean,
                     var ContinuousLens: Boolean,
                     var CameraOff: Boolean,
                     var FocusTrimCalibrationComplete: Boolean,
                     var FocusTrimCalibrationTimeout: Boolean,
                     var SubPayloadIndex: Byte,
                     var FieldOfViewIndex: Byte,
                     var FilterIndex: Byte,
                     var GateSizeIndex: Byte,
                     var SceneIndex: Byte,
                     var EZoomIndex: Byte,
                     var TemporalProcessingIndex: Byte,
                     var SpatialProcessingIndex: Byte,
                     var CameraExtenderIndex: Byte,
                     var PsuedoColorIndex: Byte,
                     var PsuedoColorThreshold: Short,
                     var HorizontalFovDegrees: Double,
                     var ZoomMillimeters: Double,
                     var FocusPosMeters: Double,
                     var FocusTrimFraction: Double,
                     var SensitivityExposureGainInDb: Double,
                     var LevelFraction: Double,
                     var EZoomValue: Double,
                     var EffectiveHorizationFovDegrees: Double





) : Status{

}