package snc.turret.mx;

import snc.turret.base.TurretBase;
import snc.turret.enums.*;
import snc.turret.mx.commands.CommandCueSensor;
import snc.turret.mx.commands.CommandSetGimbalSteeringMode;
import snc.turret.mx.commands.CommandSetVIC;
import snc.turret.status.data.*;

public class TurretMX extends TurretBase {

    @Override
    public void addLaserCode(short laserCode, boolean useSpecificSlot, byte slotNumber) {
        // TODO - Implement this
        logger.warning("Received command - addLaserCode. This is not yet implemented");
    }

    //TODO - In the A-29 MX Server, it looks like Altitude is overwritten to an invalid value to force DTED use.
    // Investigate whether we want to change this to allow user specified altitude.
    @Override
    public void cueSensor(double latitudeDegrees, double longitudeDegrees, double altitudeMeters) {
        CommandCueSensor cueCommand = new CommandCueSensor(latitudeDegrees, longitudeDegrees, altitudeMeters);

        logger.info(String.format("Cueing sensor to lat %3f lon %3f  alt %3f ",
                cueCommand.getLatitudeDegrees(),
                cueCommand.getLongitudeDegrees(),
                cueCommand.getAltitudeMeters()
        ));

        writeCommandBytes(cueCommand.getBytes());
    }

    @Override
    public void operateMenu(MenuOperation menuOperation) {
        // TODO - Implement this
        logger.warning("Received command - operateMenu. This is not yet implemented");
    }

    @Override
    public void setAVT_Size(TrackWindowSize windowSize) {
        // TODO - Implement this
        logger.warning("Received command - setAVT_Size. This is not yet implemented");
    }

    @Override
    public void setAVT_TrackingSource(AvtTrackingSource avtTrackingSource) {
        // TODO - Implement this
        logger.warning("Received command - setAVT_TrackingSource. This is not yet implemented");
    }

    @Override
    public void setExposureMode(ExposureMode newMode) {
        // TODO - Implement this
        logger.warning("Received command - setExposureMode. This is not yet implemented");
    }

    @Override
    public void setFocus(byte focusValue) {
        // TODO - Implement this
        logger.warning("Received command - setFocus. This is not yet implemented");
    }

    @Override
    public void setGainLevelAdjustMode(SensitivityGainLevelAdjustMode gainLevelAdjustMode) {
        // TODO - Implement this
        logger.warning("Received command - setGainLevelAdjustMode. This is not yet implemented");
    }

    @Override
    public void setGimbalMode(GimbalSteeringMode newMode) {

        CommandSetGimbalSteeringMode modeCommand = new CommandSetGimbalSteeringMode(newMode);

        logger.info(String.format("Setting steering mode to %1$s", modeCommand.getSteeringMode()));
        writeCommandBytes(modeCommand.getBytes());
    }

    @Override
    public void setLDRMode(LdrFunction ldrMode) {
        // TODO - Implement this
        logger.warning("Received command - setLDRMode. This is not yet implemented");
    }

    @Override
    public void setVIC(SensorType vicSensorType) {
        CommandSetVIC vicCommand = new CommandSetVIC(vicSensorType);

        logger.info(String.format("Setting VIC to %1$s", vicCommand.getSensorType()));
        writeCommandBytes(vicCommand.getBytes());
    }

    @Override
    public void setZoom(ZoomMode zoomMode, double zoomFovAngle, double zoomContinuousFraction, int zoomIndex) {
        // TODO - Implement this
        logger.warning("Received command - setZoom. This is not yet implemented");
    }

    @Override
    public void slewSensor(double azimuth, double elevation) {
        // TODO - Implement this
        logger.warning("Received command - slewSensor. This is not yet implemented");
    }

    @Override
    public void stopSlew() {
        // TODO - Implement this
        logger.warning("Received command - stopSlew. This is not yet implemented");
    }

    @Override
    public void setFeatureActivation(ActivatableFeature feature, FeatureActivationStatus featureStatus) {
        // TODO - Implement this
        logger.warning("Received command - setFeatureActivation. This is not yet implemented");
    }

    @Override
    public CommandedLaserStatus getCommandedLaserStatus() {
        // TODO - Implement this
        logger.warning("Received status request - getCommandedLaserStatus. This is not yet implemented");
        return null;
    }

    @Override
    public FusionStatus getFusionStatus() {
        // TODO - Implement this
        logger.warning("Received status request - getFusionStatus. This is not yet implemented");
        return null;
    }

    @Override
    public GimbalModeStatus getGimbalSteeringModeStatus() {
        // TODO - Implement this
        logger.warning("Received status request - getGimbalSteeringModeStatus. This is not yet implemented");
        return null;
    }

    @Override
    public LaserCoordinateStatus getLaserCoordinateStatus() {
        // TODO - Implement this
        logger.warning("Received status request - getLaserCoordinateStatus. This is not yet implemented");
        return null;
    }

    @Override
    public LaserSpotTrackerStatus getLaserSpotTrackerStatus() {
        // TODO - Implement this
        logger.warning("Received status request - getLaserSpotTrackerStatus. This is not yet implemented");
        return null;
    }

    @Override
    public LaserStatus getLaserStatus() {
        // TODO - Implement this
        logger.warning("Received status request - getLaserStatus. This is not yet implemented");
        return null;
    }

    @Override
    public MenuStatus getMenuStatus() {
        // TODO - Implement this
        logger.warning("Received status request - getMenuStatus. This is not yet implemented");
        return null;
    }

    @Override
    public SensorOrientationStatus getSensorOrientationStatus() {
        // TODO - Implement this
        logger.warning("Received status request - getSensorOrientationStatus. This is not yet implemented");
        return null;
    }

    @Override
    public SensorPayloadStatus getSensorPayloadStatus() {
        // TODO - Implement this
        logger.warning("Received status request - getSensorPayloadStatus. This is not yet implemented");
        return null;
    }

    @Override
    public SensorPositionStatus getSensorPositionStatus() {
        // TODO - Implement this
        logger.warning("Received status request - getSensorPositionStatus. This is not yet implemented");
        return null;
    }

    @Override
    public SensorTargetStatus getSensorTargetStatus() {
        // TODO - Implement this
        logger.warning("Received status request - getSensorTargetStatus. This is not yet implemented");
        return null;
    }
}
