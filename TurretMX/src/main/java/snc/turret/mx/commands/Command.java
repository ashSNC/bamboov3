package snc.turret.mx.commands;

import third.party.utility.ByteSwapper;
import snc.turret.enums.CommandID;

import java.nio.ByteBuffer;

public abstract class Command {

    public final byte[] getBytes(){
        if(messageBuffer == null){
            messageBuffer = initializeByteBuffer();
            putPayloadInByteBuffer(messageBuffer);
        }

        return messageBuffer.array();
    }

    public abstract CommandID getCommandID();

    /**
     * Fills the supplied byte buffer with data values used for command settings.
     * <p>
     * -----------IMPORTANT NOTE-----------
     * <p>
     * Since the server is expecting data in Little Endian format, and Java uses Big Endian,
     * the ByteSwapper.Swap method must be used on all multi-byte values.
     * Example: buffer.putInt(ByteSwapper.swap(myIntValue));
     *
     * @param buffer ByteBuffer to be filled with payload data.
     */
    protected  abstract void putPayloadInByteBuffer(ByteBuffer buffer);
    protected  abstract  int getPayloadSize();

    private ByteBuffer messageBuffer;
    private final ByteBuffer initializeByteBuffer(){

        int messageSize = getMessageSize();
        int totalBufferSize = messageSize + Integer.BYTES; //must allocate space for message size at the beginning of the buffer
        ByteBuffer buffer = ByteBuffer.allocate(totalBufferSize);

        buffer.putInt(ByteSwapper.swap(messageSize));
        buffer.putInt(ByteSwapper.swap(getCommandID().getValue()));
        return buffer;
    }

    private final int getMessageSize(){
        int commandID_Size = Integer.BYTES;
        return commandID_Size + getPayloadSize();
    }
}
