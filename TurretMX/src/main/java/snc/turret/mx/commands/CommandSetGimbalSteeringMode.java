package snc.turret.mx.commands;

import snc.turret.enums.CommandID;
import snc.turret.enums.GimbalSteeringMode;

import java.nio.ByteBuffer;

public class CommandSetGimbalSteeringMode extends Command {

    public CommandSetGimbalSteeringMode(GimbalSteeringMode newMode) {
        steeringMode = newMode;
    }

    private final GimbalSteeringMode steeringMode;
    public GimbalSteeringMode getSteeringMode(){
        return steeringMode;
    }

    @Override
    public CommandID getCommandID() {
        return CommandID.SET_GIMBAL_MODE;
    }

    @Override
    protected int getPayloadSize() {
        return Byte.BYTES;
    }

    @Override
    protected void putPayloadInByteBuffer(ByteBuffer buffer) {
        buffer.put(steeringMode.getValue());
    }
}
