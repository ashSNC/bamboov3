package snc.turret.mx.commands;

import snc.turret.enums.CommandID;
import third.party.utility.ByteSwapper;

import java.nio.ByteBuffer;

public class CommandCueSensor extends Command {

    public CommandCueSensor(double latitude, double longitude, double altitude) {

        latitudeDegrees = latitude;
        longitudeDegrees = longitude;
        altitudeMeters = altitude;
        courseTrueNorth = 0d;
        groundSpeedMetersPerSecond = 0d;
        timeTag = 0;
    }



    private final Double latitudeDegrees;
    public double getLatitudeDegrees(){
        return latitudeDegrees;
    }

    private final Double longitudeDegrees;
    public double getLongitudeDegrees(){
        return longitudeDegrees;
    }

    //TODO - In the A-29 MX Server, it looks like Altitude is overwritten to an invalid value to force DTED use.
    // Investigate whether we want to change this to allow user specified altitude.
    private final Double altitudeMeters;
    public double getAltitudeMeters(){
        return altitudeMeters;
    }

    private final Double courseTrueNorth;
    public double getCourseTrueNorth(){
        return courseTrueNorth;
    }

    private final Double groundSpeedMetersPerSecond;
    public double getGroundSpeedMetersPerSecond(){
        return groundSpeedMetersPerSecond;
    }

    private final Integer timeTag;
    public double getTimeTag(){
        return groundSpeedMetersPerSecond;
    }   //Microseconds past the hour

    @Override
    public CommandID getCommandID() {
        return CommandID.CUE_SENSOR;
    }

    @Override
    protected int getPayloadSize() {

        return Integer.BYTES +      //TimeTag
                (Double.BYTES * 5); //Latitude, Longitude, Altitude, Course, Ground Speed
    }

    @Override
    protected void putPayloadInByteBuffer(ByteBuffer buffer) {
        buffer.putInt(ByteSwapper.swap(timeTag));
        buffer.putDouble(ByteSwapper.swap(latitudeDegrees));
        buffer.putDouble(ByteSwapper.swap(longitudeDegrees));
        buffer.putDouble(ByteSwapper.swap(altitudeMeters));
        buffer.putDouble(ByteSwapper.swap(courseTrueNorth));
        buffer.putDouble(ByteSwapper.swap(groundSpeedMetersPerSecond));
    }
}
