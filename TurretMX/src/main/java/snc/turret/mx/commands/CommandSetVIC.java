package snc.turret.mx.commands;

import snc.turret.enums.CommandID;
import snc.turret.enums.SensorType;

import java.nio.ByteBuffer;

public class CommandSetVIC extends Command {

    public CommandSetVIC(SensorType newVIC) {
        vicType = newVIC;
    }

    private final SensorType vicType;
    public SensorType getSensorType(){
        return vicType;
    }

    @Override
    public CommandID getCommandID() {
        return CommandID.SELECT_EOIR_SENSOR;
    }

    @Override
    protected int getPayloadSize() {
        return Byte.BYTES;
    }

    @Override
    protected void putPayloadInByteBuffer(ByteBuffer buffer) {
        buffer.put(vicType.getValue());
    }
}
